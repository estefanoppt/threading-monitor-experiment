./bin/monitor: ./obj/*.o
	g++ -std=gnu++14 ./obj/*.o -pthread -Wall -I ./include/ -o $@

./obj/*.o: ./src/*.cpp
	g++ --std=gnu++14 -pthread -Wall -I ./include/ -c $^ 
	mv *.o ./obj/

.PHONY: clean
clean: 
	rm -f ./obj/*.o ./bin/* 

