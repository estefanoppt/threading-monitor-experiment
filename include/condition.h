/*
 * Look at ./include/dinning-philosophers.h explanation
 * of why we do this here.
 */
#ifndef CONDITION_H
#define CONDITION_H

#include <semaphore.h>

/*
 * Defines std::size_t. We don't need to incclude this
 * in the other files because std::size_t comes pre-declared
 * in most other header files of C and C++.
 */
#include <cstddef>		
#include "philosophers.h"

/*
 * In C and C++, to be able to declare a pointer of a certain
 * type, you only need a DECLARATION of the type, not a DEFINITION.
 * The full definition of DinningPhilosophers can be found in
 * ./include/dinning-philosophers.h and ./src/dinning-philosophers.cpp.
 */
class DinningPhilosophers; 

/* 
 * This is the Condition TDA as used by the DinningPhilosophers
 * monitor. There's one Condition device per philosopher thread,
 * and the philosopher's id is saved wihtin the Condition device.
 */
class Condition {
public:
	/*
	 * We initialize each Condition device with a pointer
	 * reference to the DinningPhilosophers monitor
	 * and the ID of the philosopher thread owner of
	 * this Condition device.
	 */
	Condition(DinningPhilosophers*, philosopher_id_t);

	/*
	 * We need to liberate a semaphore, that's why we declare a
	 * destructure here.
	 */
	~Condition();

	/*
	 * The principal functions to manage the Condition
	 * device. The philosopher_id_t here are only for debugging
	 * purposes and are functionally unnecessary.
	 */ 
	void wait(philosopher_id_t);
	void signal(philosopher_id_t);

private:

	/*
	 * This is the semaphore used by each Condition
	 * device to put a thread to asleep when its
	 * signal() method is called.
	 */
	 sem_t                       _sem;

	 /*
	  * A call to signal() on a Condition device
	  * that hasn't locked a thread needs to do
	  * nothing. So we need to keep the state
	  * of the device.
	  */
	 bool			     _isLocked;

	 /*
	  * The ID of the philosopher thread owner
	  * of this Condition device.
	  */
	 philosopher_id_t            _id;

	 /* 
	  * We need a reference to the monitor since
	  * we also need to coordinate the threads that
	  * are to be awakened as asleep there. In other
	  * words, we need to manipulate the semaphores
	  * in those routines too.
	  */
	 DinningPhilosophers*        _philosophers;

};

#endif
