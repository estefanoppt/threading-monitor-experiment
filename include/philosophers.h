#ifndef PHILOSOPHERS_H
#define PHILOSOPHERS_H

/*
 * This simple header defines a couple of
 * simple types that will be useful for
 * both the DinningPhilosophers monitor
 * and the Condition device.
 */

/*
 * We need to keep track the state of each of the
 * philosophers, and we declare this enumeration
 * to do so. 
 */
enum PhilosopherState {
	THINKING,
	HUNGRY,
	EATING
};

/*
 * This is a simple typedef that we use
 * to refer to the concept of a philospher
 * id.
 */
typedef std::size_t philosopher_id_t;

#endif
