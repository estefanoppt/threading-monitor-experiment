/*
* We are using these two macros in case the header has been included
* multiple times. If #ifndef macro skips the compilation of all code up
* until the #endif macro below if the macro in question IS NOT defined.  
* Thus, if the macro isn't defined, we subsequently define it. Hence, when
* someone tries to include the same header twice accidentaly, the declarations
* won't be repeated on the target code. I know that it seems weird that
* someone might include the same header twice, but think about what happens
* when a library header A includes a header B internally, and a source-file
* C includes A but also includes B because the source-file doesn't know A includes B
* in the first place. Here we would have an accidental double inclusion of a header.
*/
#ifndef MONITOR_H
#define MONITOR_H

#include <semaphore.h>
#include <array>
#include <memory>

/*
 * As a helper TDA to coordinate the use of the monitor, use the Condition
 * class defined in this header. 
 */
#include "philosophers.h"
#include "condition.h"

/* 
 * This is the main class of this whole project. It implements
 * a monitor that solves the Dinning Philosophers Problem. 
 * It is a class which defines two crucial functions: 
 * 	
 * 	1. pickup()
 * 	2. putdown()
 *
 * Each of these functions takes an argument 'i', which specifies
 * the ID of the Philosopher that's attempting to pickup() the chopsticks
 * or put them down.
 *
 * For each of the class methods below, there's a corresponding
 * definition in ./src/dinning-philosophers.cpp. The full explanation
 * of what each method does lies in that file. So go there for
 * more information.
 *
 * Remember that one ONE AND ONLY ONE thread of execution can
 * be awake executing ANY of the monitor's methods. How we achieve
 * this is also explained in the dinning-philosophers.cpp source file.
 */
class DinningPhilosophers {
public:
	// This is a C++ constructor, akin to Java or C# constructors.
	DinningPhilosophers();

	/*
	 * This is a C++ destructor. There is no equivalent concept in Java.
	 * In C++, since there is no garbage collection for dynamic memory,
	 * and since most other resources are unmanaged (there's no equivalent
	 * of the JVM by default for C++), we need a way to deallocate, close,
	 * and destroy any resources held by the object. When are destructors
	 * called? In these occasions:
	 *
	 * 	1. Automatic variables are destroyed when their function
	 * 	   scope ends.
	 *
	 * 	2. Dynamic variables are destroyed when 'delete' or 'free()'
	 * 	   are called.
	 *
	 * 	3. File-static (global) variables are destroyed on program 
	 * 	   termination.
	 */
	~DinningPhilosophers();
	
	/*
	 * A philosopher thread uses this function when he wants to eat and
	 * grab two chopstics.
	 */
	void pickup(philosopher_id_t i);

	/*
	 * A philosopher thread uses this function when he has stopped
	 * eating and wants to put down the chopsticks, also signaling
	 * any other waiting philosophers that they can eat.
	 */
	void putdown(philosopher_id_t i);

	/*
	 * The number of philosophers this monitor supports.
	 */
	static constexpr std::size_t N = 5;
private:

	/*
	 * A friend class in C++ is a class that can access
	 * private elements of another class. 
	 */
	friend class Condition;

	/*
	 * This here tests whether the adjacent philosophers
	 * are busy with chopstics. Don't pay much attention
	 * to the first philosopher_id_t, that's for 
	 * debugging purposes. What it means is that
	 * philosopher 'p' is _test()int for philosopher 'i'.
	 * Otherwise 'p' isn't really used inside the routine.
	 * The test is done over philosopher number ip'.
	 */
	void _test(philosopher_id_t p, philosopher_id_t i);

	/*
	 * These two function emulate the "monitor can only
	 * have one thread of execution executing on it 
	 * at any one time." You'll see these two functions
	 * called at the beginning and end of pickup()
	 * and putdown(), which makes only one thread active
	 * at any one time in those two functons.
	 */
	void _entry_section(philosopher_id_t);
	void _exit_section(philosopher_id_t);

	/*
	 * The state of each philosopher.
	 */
	std::array<PhilosopherState, N> _states;

	/*
	 * The Condition device for each philosopher. 
	 * Note that we use a std::shared_ptr (explained in Main.cpp)
	 * to hold the pointer to each Condition device within
	 * the std::array<>.
	 */
	std::array<std::shared_ptr<Condition>, N> _self;

	/*
	 * The mutex used to make _entry_section() and 
	 * _exit_section() work.
	 */
	sem_t _mutex;

	/*
	 * This semaphore is used to coordinate the awakening
	 * and sleeping of philosophers using the _self Condition
	 * array above. 
	 *
	 * _next_count counts the number of philosophers
	 * waiting, asleep, for a signaled thread to finish.
	 * That is, if a philosopher thread calls signal() to
	 * the condition device of another philosopher it, itself,
	 * needs to be put asleep so that the other philosopher (who
	 * has been recently awakened), can finish executing its
	 * monitor call. Remember: one one thread of execution
	 * can be awake at any time within the monitor, so, if
	 * a call to signal() in a condition device awakens another
	 * thread, the caller thread needs to be put to sleep
	 * for a while. 
	 */
	std::size_t _next_count;
	sem_t _next;
};

#endif
