#include "dinning-philosophers.h"

//  -----------------------------------------------------------------------------
/*
 * This is a simple, default constructor which first applies the default constructor
 * to every element in the monitor, when initializes the semaphores _mutex and _next
 * to 1 and 0 respectively, and finally initializes every philosopher to the state
 * of THINKING and the Condition device of every philosopher.
 */ 
//  -----------------------------------------------------------------------------
DinningPhilosophers::DinningPhilosophers():
	_states(), _self(), _mutex(), _next_count(0), _next(){

	sem_init(&_mutex, 0, 1);
	sem_init(&_next, 0, 0);

	for (std::size_t i = 0; i < N; i++){
		_states[i] = THINKING;

		/*
		 * C++ favors the use of operator 'new' instead of calling 'malloc',
		 * but both disciplines could've been done here. Operator 'new' works
		 * pretty much like its Java counterpart, but returns a pointer
		 * to dynamic memory instead of a reference. Note that the
		 * pointer returned by new is being saved in a std::shared_ptr<>, which
		 * means that it will be automatically deallocated once the DinningPhilosophers
		 * monitor is destroyed. Had we NOT used std::shared_ptr<>, we would've had
		 * to manually 'delete'd each pointer in this class's destructor.
		 */
		_self[i]   = std::shared_ptr<Condition>(new Condition(this, i));
	}
}


//  -----------------------------------------------------------------------------
/*
 * This simple destructor deallocates the resources of each semaphore.
 */
//  -----------------------------------------------------------------------------
DinningPhilosophers::~DinningPhilosophers(){
	sem_destroy(&_mutex);
	sem_destroy(&_next);
}


//  -----------------------------------------------------------------------------
/*
 * This is one of the two most important routines in the entire project. This routine,
 * pickup(), basically makes the philosopher attempt to pick BOTH chopsticks at the same
 * time. If either or both chopsticks are unavailable, the philosopher goes to sleep until
 * the EATING philosophers signal this sleeping philosopher to continue.
 */
//  -----------------------------------------------------------------------------
void DinningPhilosophers::pickup(philosopher_id_t p){

	printf("\tP[%zu] in pickup(%zu).\n", p, p);
	
	/*
	 * This forces the monitor to allow only one thread of execution
	 * at a time between all its methods.
	 */
	_entry_section(p);
	printf("\tP[%zu] back in pickup(%zu) from _entry_section(%zu).\n", p, p, p);
	
	//
	//
	//
	// Actual functionallity between these bars.
	// -----------------------------------------------------------------------------

	/*
	 * We change the state to HUNGRY, and call _test() to see whether
	 * the chopsticks are available. Don't worry about. Don't pay
	 * attention to the second 'p' argument, that's there for debugging
	 * purposes (this is fully explained in ./src/condition.cpp.  
	 */
	printf("\tP[%zu], _states[%zu] = HUNGRY.\n", p, p);
	_states[p] = HUNGRY;
	_test(p, p); // Testing from philosopher 'p' to check philosopher 'p'.

	printf("\tP[%zu] back in pickup(%zu) from _test(%zu, %zu).\n", p, p, p, p);
	
	if (_states[p] != EATING){
		printf("\tP[%zu], _state[%zu] != EATING\n", p, p);
		
		// If state isn't EATING, it's because chopstics where busy, so we go to sleep.
		_self[p]->wait(p);

		printf("\tP[%zu] back in pickup(%zu) from _self[%zu]->_wait().\n", p, p, p);
	} else {
		// If state IS EATING, it's because chopsticks were free, so we return (the
		// else here is just for debugging purposes, and its functionally 
		// unnecessary.
		printf("\tP[%zu], _state[%zu] == EATING, do nothing else.\n", p, p);
	}
	// -----------------------------------------------------------------------------
	// Actual functionallity between these bars.
	//
	//
	//
	

	// Releases mutex so that other threads of execution can continue executing
	// any other monitor method.
	_exit_section(p);
	printf("\tP[%zu], back in pickup(%zu) from _exit_section(%zu).\n", p, p, p);
}


//  -----------------------------------------------------------------------------
/*
 * This method encapsulates a philosopher putting down the chopsticks, and
 * signaling the philosophers beside him that the chopsticks are available.
 * This is the second most important routine in the entire project.
 */
//  -----------------------------------------------------------------------------
void DinningPhilosophers::putdown(philosopher_id_t p){
	printf("\tP[%zu] in putdown(%zu).\n", p, p);

	/*
	 * This forces the monitor to allow only one thread of execution
	 * at a time between all its methods.
	 */
	_entry_section(p);
	printf("\tP[%zu] back in putdown(%zu) from _entry_section(%zu).\n", p, p, p);

	//
	//
	//
	// Actual functionallity between these bars.
	// -----------------------------------------------------------------------------
	printf("\tP[%zu], _state[%zu] = THINKING\n", p, p);

	// Since we're putting the chopsticks down, this philosopher changes to THINKING.
	_states[p] = THINKING;

	// We execute test on adjacent philosophers, which, if _test() finds that 
	// the philosophers can, in fact, grab the chopsticks and eat, then changes
	// their state and signals their threads to resume execution.
	philosopher_id_t N_0 = (p + 4) % N;
	_test(p, N_0);
	printf("\tP[%zu] back in putdown(%zu) from _test(%zu, %zu).\n", p, p, p, N_0);

	philosopher_id_t N_1 = (p + 1) % N;
	_test(p, N_1);
	printf("\tP[%zu] back in putdown(%zu) from _test(%zu, %zu).\n", p, p, p, N_1);
	
	// -----------------------------------------------------------------------------
	// Actual functionallity above this line.
	//
	//
	//
	
	// Releases mutex so that other threads of execution can continue executing
	// any other monitor method.
	_exit_section(p);
	printf("\tP[%zu] back in putdown(%zu) from _exit_section(%zu).\n", p, p, p);
}


//  -----------------------------------------------------------------------------
/*
 *  This routine abstracts the notion that philosopher 'p' is testing
 *  whether philosopher 'i' can grab the chopsticks. If philosopher 'i' in 
 *  fact CAN grab the chopsticks (because they're free), when we change that
 *  philosopher's state and signal its thread to begin execution.
 *
 *  Note that we don't have to call _entry_section() nor _exit_section() since
 *  _test() is private, and its only called from pickup() and putdown(), which
 *  already have exclusive access.
 */
//  -----------------------------------------------------------------------------
void DinningPhilosophers::_test(philosopher_id_t p, philosopher_id_t i){
	printf("\t\tP[%zu] in _test(%zu, %zu).\n", p, p, i);

	// Calculate adjacent philosopher IDs.
	philosopher_id_t right = (i + 4) % N;
	philosopher_id_t left  = (i + 1) % N;

	/*
	 * If philosophers adjacent to philosopher 'i' are not eating, 
	 * this means their chopsticks are free, so we let the philosopher 'i'
	 * grab the chopstick and change state to EATING. We also signal its thread
	 * to continue (remember that Philosopher 'i' went to sleep in his call
	 * to pickup()).
	*/
	if ((_states[right] != EATING) && (_states[i] == HUNGRY) && (_states[left] != EATING)) {
		printf("\t\t_states[%zu] != EATING and _states[%zu] == HUNGRY and _states[%zu] != EATING.\n", right, i, left);

		printf("\t\tP[%zu], _state[%zu] = EATING.\n", p, i);
		_states[i] = EATING;

		/*
		 * If this call to _test() comes from a putdown() by another philosopher thread
		 * (that is to say, 'p' =! 'i', then we need to wake this philosopher thread. If,
		 * however, this call to _test() comes from pickup() (that is to say, 'p' == 'i'),
		 * then this signal() does nothing since that philosopher thread is already awake.
		 * Note that the current philosopher will be put to sleep temporarilly by this 
		 * signal() call, as this call will awaken another philosopher who's going to
		 * return to pickup(). Since only one thread must be awake at any time on a monitor,
		 * this is totally necessary.
		 */
		_self[i]->signal(p);
		printf("\t\tP[%zu] back in _test(%zu, %zu) from _self[%zu]->signal(%zu).\n", p, p, i, i, p);

	} else {
		/*
		 * If chopsticks are still busy, do nothing. This 'else' is thus functionally
		 * unnecessary, and is here so that we can output debugging information
		 * to stdout.
		 */
		printf("\t\t_states[%zu] == EATING or _states[%zu] != HUNGRY or _states[%zu] == EATING.\n", right, i, left);
		printf("\t\tP[%zu], do nothing in _test(%zu, %zu)\n", p, p, i);
	}
}


//  -----------------------------------------------------------------------------
/*
 * This function is used in pickup() and putdown() to force only one thread
 * of execution to be active in the monitor. Semantically, this means a new
 * thread has entered a monitor's routine.
 */
//  -----------------------------------------------------------------------------
void DinningPhilosophers::_entry_section(philosopher_id_t p){
	printf("\t\tP[%zu] in _entry_section(%zu).\n", p, p);
	printf("\t\t!!!P[%zu] does sem_wait(&_mutex).\n", p);
	sem_wait(&_mutex);
}


//  -----------------------------------------------------------------------------
/*
 * This function enables another (blocked) thread to execute either
 * pickup() and putdown(). Semantically, this means a thread has
 * exited any of the monitor's routines.
 */
//  -----------------------------------------------------------------------------
void DinningPhilosophers::_exit_section(philosopher_id_t p){

	printf("\t\tP[%zu] in _exit_section(%zu).\n", p, p);

	if (_next_count > 0){

		/*
		 * If the current philosopher has been awakened by a call to
		 * signal() from another philosopher, then we must awake that
		 * philosopher before exiting.
		 */
		printf("\t\tP[%zu], _next_count > 0 (%zu).\n", p, _next_count);
		printf("\t\t~~~P[%zu] does sem_post(&_next).\n", p);
		sem_post(&_next);

	} else {

		/*
		 * If there are no asleep philosophers due to a fall to 'signal()',
		 * then just wake any philosophers that might be asleep in the _entry_section()s
		 * of pickup() or putdown().
		 */

		printf("\t\tP[%zu], _next_count <= 0 (%zu).\n", p, _next_count);
		printf("\t\t~~~P[%zu] does sem_post(&_mutex).\n", p);
		sem_post(&_mutex);
	}
}



