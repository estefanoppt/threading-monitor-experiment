// README --------------------------------------------------------------------
/*
 * AUTOR:
 * 	estefano-ppt
 *
 * GREETINGS:
 * 	This project seeks to implement the solution of the Dinning Philosophers 
 * 	Problem using monitors, as explained in Section 9.6.2, 9.6.3 of Operating 
 * 	System Concepts, 9th Edition, from Silberschatz et al. Since monitors
 * 	require the use of ADTs (abstract data-structures), I've chosen to 
 * 	implement this solution in C++14. In the comments that follow, however,
 * 	I'll explain every C++ feature that might be unknown for an ANSI C 
 * 	programmer. I do this because I know that ESPOL students come from a 
 * 	good background of ANSI C, as they come from the Systems Programming
 * 	class in semesters prior to the Operating Systems class. Do not fret,
 * 	if you know Java, C#, or any other object-oriented laguage, you will
 * 	be able to understand this pretty well.
 *
 * 	This project uses:
 *
 * 	1. POSIX Threads: 
 * 		The standard threading library for UNIX ang GNU/Linux systems.
 *
 * 	2. POSIX Semaphores
 *
 * 	3. POSIX Mutexes
 *
 * 	4. C++11 Standard std::array<>: 
 * 		A statically-bound array structure with a cleaner interface
 * 	        than raw C-style arrays. These arrays have a static
 * 	        size that's defined at compile at. That is to say, these
 * 	        arrays are NOT dynamic at all, they can't grow beyond their
 * 	        initial size.
 *
 * 	5. C++11 Standard std::shared_ptr<>
 * 		A so-called smart pointer which has internal reference
 * 		counting and automatically frees the memory of the underlying
 * 		object when there are not more references to that memory.
 *
 * WHY ALL THE printf()s?
 *	Apart from seeking to display a denmonstration of an implementation
 *	of monitors, this program also seeks to let students understand the
 *	vagaries of the short-term scheduling, and the way the mutexes
 *	and semaphores awake and sleep threads. If you study the output of
 *	this program carefully, you will come to understand a lot about threads
 *	and CPU scheduling.
 *
 * HOW TO GO ABOUT IT
 * 	If I were you sitting there trying to understand this for the
 * 	first time, I would read the files in the following order:
 *
 * 	1. ./src/main.cpp
 *	2. ./inclulde/philosophers.h
 *	3. ./include/dinning-philosophers.h
 *	4. ./src/dinning-philosophers.cpp
 *	5. ./include/condition.h
 *	6. ./src/condition.cpp
 *
 *	Read in this order, the explanations for each concept will 
 *	flow nicely and linearly. You're free to approach your
 *	studying of this code however you choose to, however. Now, I wouldn't
 *	try to understand every perfectly well on the first run. I would
 *	read it all, then read it again. Most questions will be asnwered
 *	with two readings of this project.
 *
 * A NOTE ON C++
 * 	Remember, C++ is built on top of C89, so that everything that's
 * 	doable and possible in C89 is also doable and possible in C++. Yes,
 * 	when used with all the new features, C++ can be viewed as an entirely 
 * 	new language, but one of its main powers is that you can always get
 * 	down to write traditional C-stype code when need arises. This makes
 * 	C++ compatible with virtually every single line of existing C code
 * 	in existance. 
 *
 * WHY ENGLISH?
 * 	Because neither standard ANSI C nor C++14 allow unicode characters
 * 	in the program text segment, which means I can't put accent characters
 * 	to vowels in spanish. I need my tildes man.
 *
 * LICENSE:
 * 	This code is subject to Estefano's do-whatever-you-f'ing-want-with-it
 * 	license. If you wish to repay the favor, introduce me to any girl friends
 * 	that you might have. Girls are cute. I want to meet them all.
 */


// INCLUDES  --------------------------------------------------------------------
/*
 * These arae C++ standard headers. Not how they don't need the '.h' at
 * the end. Also note the header cstdio. This is C++'s way of refering to
 * the tradicional ANSI C stdio.h library header. Every traditional ANSI C
 * library header (e.g., stdlib.h, string.h, stdio.h) can be brought into
 * C++ by removing the trailing '.h' and appending a 'c' before the name
 * (e.g., cstdlib, cstring, cstdio).  
 */
#include <cstdio>
#include <array>

// UNIX Standard Library Header
#include <unistd.h>

#include "dinning-philosophers.h"


// GLOBAL VARIABLES  ------------------------------------------------------------
// This is the declaration of the monitor TDA that we will use in this
// program. It's full definition can be found in ./src/dinning-philosophers.cpp
// and its declaration in ./include/dinning-philosophers.h.
DinningPhilosophers philosophers_monitor;

// This is just a loop count that estimates the amount of time that the
// philosopher takes in "dinning". You'll understand this better below.
constexpr std::size_t EATING_LOOP_SIZE = 50;


// FUNCTIONS  -------------------------------------------------------------------
// Forward-declaration for the function that each phisopher thread will
// run. It's definition is below.
void* eat(void* arags);

// Our main() function. It doesn't really take any commad-line arguments.
// The function basically creates N philosopher threads, and waits to
// join them.
int main(int argc, char* argv[]){

	/* 
	 * This declares. the pthread_t objects that will manage each 
	 * philosopher thread. The '::' notation here is C++'s namespace 
	 * scope-resolution mechanism. For example, std::array<> says 
	 * "get the array<> type in the std namespace", and the 
	 * Dinninghilosophers::N says "get the N static variable from the 
	 * DinningPhilosophers class." 
	 */
	std::array<pthread_t, DinningPhilosophers::N> philosopher_threads;

	/*
	 * This declares a sort of 'ID' for each philosopher. We need this so 
	 * that we can make sense of the program's output: we want to know 
	 * which philosopher a particular printf() refers too. 
	 */
	std::array<philosopher_id_t, DinningPhilosophers::N> IDs = {0, 1, 2, 3, 4};

	/*
	 * This basically creates each of the N philosopher threads. 
	 *
	 * Look how we send each of the philosopher_threads[] items to
	 * the pthread_create() fucntion: we use philosopher_threads.data() + i,
	 * which is aking to using &philosopher_threads[i] on a traditional
	 * C-style array. We do the same for the IDs std::array<>.
	 *
	 * An important aspect to important to understand here is that the
	 * IDs std::array<> is declared on this thread's (the main thread)
	 * stack. We can rely on this array to continue to exist for the
	 * other threads because we are doing pthread_join() below. Suppose
	 * this were another thread (not the main thread). If we were to
	 * create the philosopher threads in this other thread, and we
	 * failed to do pthread_join(), the thread would terminate when
	 * it reaches the end of the function, which would deallocate
	 * the stack, which in turn would deallocate IDs std::array<>,
	 * which would make the data in all the other philosopher threads
	 * unavailable for use.  However, since this is the main thread
	 * (not some other secondary thread), by the time we hit return
	 * below the underlying runtime function that called main() would
	 * call exit(), which would terminate all threads.
	 *
	 * Finally, not the 'nullptr' keyword. This is akin to using
	 * the NULL macro or the 0 literal in traditional ANSI C, but it
	 * is more typesafe, since 'nullptr' changes its underlying type
	 * to the expected pointer type of the context in question. As such,
	 * it is recommended in all C++ code to use 'nullptr' instead of
	 * the traditional null-pointer version of traditional ANSI C.
	 */
	for (std::size_t i = 0; i < DinningPhilosophers::N; i++){
		pthread_create(philosopher_threads.data() + i, nullptr, eat, IDs.data() + i);
	}

	/*
	 * This part is crucial. Were we NOT to join with the threads, the 
	 * main thread will hit the return-statement following this look, and the 
	 * process runtime would call the exit() function, which would 
	 * destroy all running threads.
	 *
	 * Note the std::size_t here. Remember, size_t was C's datatype for
	 * holding items that are countable, as such, it is usually an unsinged
	 * int or an unsigned long, depending on the underlying architecture.
	 * We append the 'std::' namespace-lookup operaator because, though
	 * we CAN use in C++ a simple size_t, good C++ practice dictates
	 * that we should make visible the namespace from which a name
	 * comes from to make reading code easier and self-documenting.
	 */
	for (std::size_t i = 0; i < DinningPhilosophers::N; i++){
		pthread_join(philosopher_threads[i], nullptr);
	}

	return 0;
}


/*
 * This is the function that each philosopher thread will run. The whole
 * point in using monitors is to simplify the underlying logic of these
 * sort of functions: if you wake away the printf()s, you can see that
 * the code is really simple, and expresses sucintly the logic behind 
 * this function:
 *
 * 	1. Philosopher picks up two chopsticks.
 * 	2. Philosopher eats.
 * 	3. Philosopher puts down chopstick.
 *
 * 	If there's no chopstick available in (1), then philosopher
 * 	waits.
 */
void* eat(void* args){
	// That reinterpret_csat<>() looks ugly, and its supposed to. 
	// These are the so called C++-style casts. In ANSI C, we could've
	// simply gotten away with doing (size_t*)args to cast the pointer,
	// but though in C++ this method is also valid, correct and idiomatic
	// C++-usage favors the use of reinterpret_cast<>() to do pointer casts
	// that are potentially dangerous. 
	std::size_t* N_ptr = reinterpret_cast<std::size_t*>(args);

	while (true){
		/*
		 * Look here how every printf() from now on has a P[%zu] on the 
		 * beginning of each line. This allows us to interpret every line
		 * of output as comming from a specific philosopher thread.
		 */
		std::printf("P[%zu] initiating while(true).\n", *N_ptr);

		/* 
		 * Here the philosopher attempts to 'pickup' the chopsticks through the
		 * monitor. This call blocks if the adjacet chopsticks are being used
		 * by other philosophers. When this call returns, the philosopher
		 * WILL SURELY HAVE TWO CHOPSTICKS ACQUIRED FOR USE.
		 */
		std::printf("P[%zu] call to philosophers_monitor.pickup(%zu).\n", *N_ptr, *N_ptr);
		philosophers_monitor.pickup(*N_ptr);

		/* 
		 * This is just some silly code to emulate the time that elapses 
		 * as the philosophers eat.
		 */
		
		for (std::size_t i = 0; i < EATING_LOOP_SIZE; i++){
			std::printf("P[%zu] EATING, i = %zu\n", *N_ptr, i);
		}

		/*
		 * Once the philosopher is done eating, he puts down the chopsticks he 
		 * has reserved for use. This wakes up other philosopher threads who might
		 * have blocked, waiting for chopsticks to be freed.
		 */
		std::printf("P[%zu] call to philosophers_monitor.putdown(%zu).\n", *N_ptr, *N_ptr);
		philosophers_monitor.putdown(*N_ptr);

		std::printf("P[%zu] ending while (true).\n\n", *N_ptr);
	}

	// There's no return value, so we return the null-pointer.
	return nullptr;
}
