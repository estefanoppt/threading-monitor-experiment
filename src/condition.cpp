#include "condition.h"
#include "dinning-philosophers.h"



//  -----------------------------------------------------------------------------
/*
 * A simple constructor that initializesthe semaphre _sem to an inital
 * state of locked.
 */
//  -----------------------------------------------------------------------------
Condition::Condition(DinningPhilosophers* philosophers, std::size_t id):
	_sem(), _isLocked(false), _id(id), _philosophers(philosophers){
	
	// Initialize _sem to a state of locked.
	sem_init(&_sem, 0, 0);	
}


//  -----------------------------------------------------------------------------
/*
 * Destructor that liberates the semaphore resource.
 */
//  -----------------------------------------------------------------------------
Condition::~Condition(){
	sem_destroy(&_sem);
}


//  -----------------------------------------------------------------------------
/*
 * A LITTLE BIT OF WARNING:
 * The following two functions are the HARDEST functions in the entire project
 * (though not the most important). I'll do my best to explain them as best as 
 * possible, but nothing beats analyzing them yourselves, helping yourselves with
 * the program's debugging out put, and come to understand these functions on your
 * own terms.
 */
//  -----------------------------------------------------------------------------



//  -----------------------------------------------------------------------------
/* 
 * This function puts a philosopher thread to sleep. If there are other 
 * philosophers waiting asleep, either at the start of either pickup()
 * and putdown() or asleep because they called signal() on another 
 * philosopher thread, this thread wakes them up.
 */
//  -----------------------------------------------------------------------------
void Condition::wait(philosopher_id_t p){
	printf("\t\t\tP[%zu], entering _self[%zu].wait(%zu).\n", p, _id, p);

	/*
	 * Since a call to wait() means that a philosopher thread will lock,
	 * we change _isLocked to true. This is important, because calls
	 * to signal() to threads which aren't locked should do nothing.
	 */
	_isLocked = true;
	printf("\t\t\tP[%zu], _self[%zu]._count++, _self[%zu]._isLocked = %s\n", p, _id, _id, _isLocked ? "true" : "false");


	if (_philosophers->_next_count > 0){
		/*
		 * If there are any philosophers that have been put to sleep because they
		 * called signal() on another philosopher, this awakens those philosophers.
		 * Read Condition::signal() for a more in-depth explanation of this.
		 */
		printf("\t\t\tP[%zu], _self[%zu]._philosophers->_next_count > 0 (%zu)\n", p, _id, _philosophers->_next_count);
		printf("\t\t\t~~~P[%zu] does sem_post(&_philosophers->_next).\n", p);
		sem_post(&_philosophers->_next);
	} else {
		/*
		 * If there aren't ny philosophers who have gone to sleep due to a call to signal(),
		 * then surely there will be philosophers waiting to be
		 * awakened at the beginning of either pickup() and putdown(). 
		 */ 
		printf("\t\t\tP[%zu], _self[%zu]._philosophers->_next_count <= 0 (%zu)\n", p, _id, _philosophers->_next_count);
		printf("\t\t\t~~~P[%zu] does sem_post(&_philosophers->_mutex).\n", p);
		sem_post(&_philosophers->_mutex);
	}

	/*
	 * Put to asleep the philosopher thread invoking this Condition device.
	 */
	printf("\t\t\t!!!P[%zu] does sem_wait(&_sem).\n", p);
	sem_wait(&_sem);

	/*
	 * Oncea philosopher awakes this philosopher through a call to signal(),
	 * we need to reset the Condition device to unlocked.
	 */
	printf("\t\t\tP[%zu] _self[%zu]._count--, self[%zu]._isLocked = %s\n", p, _id, _id, _isLocked ? "true" : "false");
	_isLocked = false;
}

//  -----------------------------------------------------------------------------
/*
 * This function awakens a philosopher thread that was put to sleep
 * through a call to this Condition device's wait() method. It does nothing
 * if the thread is not asleep. The 'p' argument here is only used for
 * debugging purposes (to refer to the philosopher that's calling signal()).
 * It is functionally unnecessary.
 */
//  -----------------------------------------------------------------------------
void Condition::signal(philosopher_id_t p){
	printf("\t\t\tP[%zu], entering _self[%zu].signal(%zu).\n", p, _id, p);

	/* 
	 * If this Condition device has been previously locked by a call
	 * to wait(), when we proceed.
	 */
	if (!_isLocked){
		printf("\t\t\tP[%zu], _self[%zu]._isLocked = %s.\n", p, _id, _isLocked ? "true" : "false");

		/*
		 * Since philosopher thread 'p' is calling this philosopher's Condition
		 * device, we need to put asleep philosopher 'p' temporarilly (since only
		 * one philosopher thread can be awake at any time). If you look at the
		 * definition of DinningPhilosophers::_exit_section(), you'll see that
		 * there's an if-statement asking if _philosophers->_next_count is bigger
		 * than zero. When this philosopher executes that if, it will awake 
		 * philosopher 'p' that's about to be put asleep.
		 */
		_philosophers->_next_count++;
		printf("\t\t\tP[%zu], _self[%zu]._philosophers->_next_count++, %zu\n", p, _id, _philosophers->_next_count);

		/*
		 * This semaphore post() awakens the philosopher that was
		 * put to sleep by a previous call to this Condition device's
		 * call to wait().
		 */
		printf("\t\t\t~~~P[%zu] does sem_post(&_sem).\n", p);
		sem_post(&_sem);

		/*
		 * Once the philosopher 'p' is awakened, we need to put the calling
		 * philosopher to sleep temporarily.
		 */
		printf("\t\t\t!!!P[%zu] does sem_wait(&_philosophers->_next).\n", p);
		sem_wait(&_philosophers->_next);

		/*
		 * Once philosopher 'p' awakens this philosopher's thread, we need to
		 * decrement DinningPhilosophers::_next_count so that other threads
		 * don't mistakenly awaken the wrong thread.
		 */
		_philosophers->_next_count--;
		printf("\t\t\tP[%zu] does _self[%zu]._philosophers->_next_count--, %zu\n", p, _id, _philosophers->_next_count);
	} else {
		/*
		 * If the Condition device isn't locked, we do nothing. This else-statement
		 * is thus functionally unnecessary, and is put here only for debugging
		 * purposes.
		 */
		printf("\t\t\tP[%zu], _self[%zu]._isLocked <= 0, do nothing.\n", p, _id);
	}
}

